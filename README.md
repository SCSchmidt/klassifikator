# Winter School `KlassifikatoR`

<!-- 
cd /home/fon/daten/ereignis/2020/klassifikatoR
pandoc -s -f   markdown -t  html   README.md   -o   flyer_klassifikatoR.html
pandoc -s -f   markdown -t  latex   README.md   -o   flyer_klassifikatoR.pdf  --latex-engine=xelatex
pandoc -s -f   markdown -t  odt   README.md   -o   flyer_klassifikatoR.odt
 --> 

## Metadaten

- Datum: 
    - Durchführungszeitraum: 09.03.2020 - 12.03.2020
    - Bewerbungsdeadline: 31.12.2019
    - Annahmebestätigung: 15.01.2020
- Ort: 
    - Christian-Albrechts-Universität zu Kiel
    - Leibnizstraße 1, Raum 205,  24118 Kiel
- Sprachen: 
    - Kurssprache: Deutsch
    - Skriptsprache: R
- Personen: 
    - Organisatoren: Oliver Nakoinz (Kiel)
    - Gastdozenten: 
        - Sophie Schmidt (Köln)
        - Martin Hinz (Bern)
        - Georg Roth (Berlin)
- Institutionen: 
    - [ISAAK](https://isaakiel.github.io)
    - [Johanna Mestorf Academy](http://www.jma.uni-kiel.de/en) 
    - [SFB 1266/A2](http://www.sfb1266.uni-kiel.de/de)
- Finanzierung: [Perle](http://www.perle.uni-kiel.de/de)
- Link zu dieser Seite: [https://gitlab.com/oliver.nakoinz/klassifikator](https://gitlab.com/oliver.nakoinz/klassifikator)
- Kontaktadresse: mod@gshdl.uni-kiel.de
- [Flyer](./4figures/Flyerentwurf8.pdf)

## Beschreibung

Klassifikationsmethoden spielen in vielen Disziplinen eine zentrale Rolle. Im Laufe dieses Kurses werden grundlegende Kenntnisse zu vier Varianten der Clusteranalyse erarbeitet. Das besondere an diesem Kurs ist, dass die Dozenten zwar die entsprechenden Informationen zu den Verfahren vorstellen, aber kein Kursmaterial bereitstellen. Nach dem Motto "Lernen durch Lehren" ist es die Aufgabe der Kursteilnehmer das Kursmaterial in Form eines Tutorials selbst zusammenzustellen. Diese Herausforderung zwingt nicht nur, sich in das Thema zu vertiefen und durch den Perspektivwechsel zwischen Lernenden und Lehrenden einen differenzierten Blick auf das Thema zu erlangen, sondern ermöglicht durch die Tutorials, die mit Zustimmung der Teilnehmer veröffentlicht werden, auch die zukünftige Lehre zu beeinflussen. Von diesem Experiment erwarten wir einen nachhaltigeren Lerneffekt als von traditionellen Ansätzen. Eigene Daten der Teilnehmer sind für Fallstudien willkommen. Grundkenntnisse in R werden erwartet. Der Kurs `KlassifikatoR` wendet sich an Studierende, Doktorandinnen und Doktoranden sowie Postdocs. 

Die Plätze sind begrenzt. Bewerbungen mit einem kurzen Motivationsschreiben und der ausdrücklichen Nennung von zwei Clusterverfahren, die thematisiert werden sollen, bitte bis zum **31.12.2019** an: mod@gshdl.uni-kiel.de. Eine Teilnahmebestätigung wird bis zum 15.01.2020 versandt.
 
## Programm

- Montag, 09.03.2020: 
    - 09:00 Begrüßung und Ablauf (Sophie, (Martin), Oliver)
    - 09:30 Einführung in die Klassifikationen (Oliver) [Präsentation](https://filedn.com/lzc4WR5d3RvmwBeqWFxkXyf/praes/nakoinz_klassifikatoR_intro.html)
    - 10:00 Rmarkdown, git, gitlab (Sophie) [Präsentation](https://filedn.com/lzc4WR5d3RvmwBeqWFxkXyf/praes/schmidt_git-einfuehrung.html) und [Präsentation slidy](https://filedn.com/lzc4WR5d3RvmwBeqWFxkXyf/tutorials/schmidt_git-einfuehrung_slidy.html)
    - 11:00 Tutorials (Michael) [sna Tutorial](https://filedn.com/lzc4WR5d3RvmwBeqWFxkXyf/tutorials/sna_ceramics_en_v06.html) und [tutorial template](https://gitlab.com/oliver.nakoinz/klassifikator/-/blob/master/5tutorials/tutorial_manual_v01.rmd)
    - 12:00 Mittagspause
    - 13:00 Arbeitsgruppen
    - 16:00 Zusammentragen und Diskussion der Ergebnisse
- Dienstag, 10.03.2020: 
    - 09:00 Distanzmaße (Georg)
    - 10:00 Arbeitsgruppen
    - 12:00 Mittagspause
    - 13:00 hDBScan (Sophie/Martin) [Präsentation](https://filedn.com/lzc4WR5d3RvmwBeqWFxkXyf/praes/schmidt_hDBscan.html) und [Präsentation slidy](https://filedn.com/lzc4WR5d3RvmwBeqWFxkXyf/praes/schmidt_hDBscan_slidy.html)
    - 14:00 Arbeitsgruppen
    - 16:00 Zusammentragen und Diskussion der Ergebnisse
- Mittwoch, 11.03.2020: 
    - 09:00 Fuzzy clustering (Oliver) [Präsentation](https://filedn.com/lzc4WR5d3RvmwBeqWFxkXyf/praes/schmidt_hDBscan_slidy.html)
    - 10:00 Arbeitsgruppen
    - 12:00 Mittagspause
    - 13:00 Arbeitsgruppen
    - 16:00 Zusammentragen und Diskussion der Ergebnisse
- Donnerstag, 12.03.2020: 
    - 09:00 Validierungsmöglichkeiten (Georg)
    - 10:00 Arbeitsgruppen
    - 12:00 Mittagspause
    - 13:00 Arbeitsgruppen
    - 16:00 Zusammentragen und Diskussion der Ergebnisse
- Freitag, 13.03.2020 (Zusatztag): 
    - 09:00 Arbeitsgruppen
    - 12:00 Mittagspause
    - 13:00 Arbeitsgruppen
    - 15:00 Zusammenfassung, Kritik und Ausblick (Sophie, Martin, Georg, Oliver)
    - 16:00 Abschlussdiskussion

Arbeitsplan zur Fertigstellung der Tutorials auf Grundlage der erarbeiteten Entwürfe:

- Fertigstellen des Inhalts bis Mitte April
- Fertigstellen der Graphen
- Interne Begutachtung (Georg) bis Ende Mai
- Übersetzung En (Sarah), FR (Carole), De (Sophie)
- Externe Begutachtung bis Ende Juli
- Überarbeiten und Abschließen der Tutorials (bis Ende August)
- Publikation auf Zenodo bis Ende September 2020

