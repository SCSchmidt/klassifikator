[33mcommit 1c3f75d227fdecc8c80f01d4587c0d883eb782a2[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Merge: 6ee1a63 7882bb8
Author: Robert Staniuk <rstaniuk@gmail.com>
Date:   Mon Mar 9 11:34:52 2020 +0100

    Merge branch 'master' of https://gitlab.com/oliver.nakoinz/klassifikator

[33mcommit 7882bb84c937d07f42507ed67c930ca90c1f433b[m
Merge: aae0380 e164ccb
Author: rplath <stu203295@mail.uni-kiel.de>
Date:   Mon Mar 9 11:33:37 2020 +0100

    Merge branch 'master' of https://gitlab.com/oliver.nakoinz/klassifikator

[33mcommit e164ccb27377aeb92d339bdcbdcb4a80a9a2d230[m
Merge: 8fbd708 526f4e0
Author: sjmartini <sjmartin16@gmail.com>
Date:   Mon Mar 9 11:33:17 2020 +0100

    Merge branch 'master' of https://gitlab.com/oliver.nakoinz/klassifikator

[33mcommit 526f4e066935c8d4bd4f00b0433c82fa104b98b4[m
Author: Georg Roth <georg.roth@fu-berlin.de>
Date:   Mon Mar 9 11:31:12 2020 +0100

    hellofromgr

[33mcommit 6ee1a638e0f7a930130de6dcf19cb480789cd052[m
Author: Robert Staniuk <rstaniuk@gmail.com>
Date:   Mon Mar 9 11:22:26 2020 +0100

    robert test

[33mcommit 8fbd708da5ddb667a1f2b72ff344f8c2c8049f3f[m
Author: sjmartini <sjmartin16@gmail.com>
Date:   Mon Mar 9 11:20:56 2020 +0100

    sarah test

[33mcommit aae03803a49e3e3f36ebb5f6c65c36da086a6016[m
Author: rplath <stu203295@mail.uni-kiel.de>
Date:   Mon Mar 9 11:17:53 2020 +0100

    test-datei von romy

[33mcommit 057be7c07225bd61df52bbf904cc1006f74ac81b[m
Author: OliverNakoinz <oliver.nakoinz@mail.de>
Date:   Mon Mar 9 11:10:15 2020 +0100

    text oliver

[33mcommit b35d1ca87b64dc9b3cfccd79aefc14a3c3574bf7[m
Author: OliverNakoinz <oliver.nakoinz@mail.de>
Date:   Mon Mar 9 11:07:46 2020 +0100

    8diverses

[33mcommit baf9587182000aa5bcca622741d312fe186739de[m
Author: OliverNakoinz <oliver.nakoinz@mail.de>
Date:   Mon Mar 9 06:13:05 2020 +0100

    readme und tutorial template

[33mcommit aee53f601c5ea25ab50e37f19678d725e83bf6e9[m
Author: Sophie C. Schmidt <s.c.schmidt@uni-koeln.de>
Date:   Sun Mar 8 21:29:24 2020 +0100

    slidy for git-Einführung

[33mcommit eb61ab201f6f7183811794fce5f11b9517082a55[m
Author: Oliver Nakoinz <oliver.nakoinz@mail.de>
Date:   Sat Mar 7 13:55:31 2020 +0100

    readme links

[33mcommit e639b88ec65895d21527a2d2a0c060c072efe2ca[m
Author: Oliver Nakoinz <oliver.nakoinz@mail.de>
Date:   Sat Mar 7 13:53:05 2020 +0100

    readme links

[33mcommit 4eac4785a9ee5b7dc837b8d277671b38f4745f7b[m
Author: Oliver Nakoinz <oliver.nakoinz@mail.de>
Date:   Sat Mar 7 13:48:43 2020 +0100

    readme

[33mcommit b2177a777a68cfbf6180b61015c7682e10080054[m
Author: SCSchmidt <s.c.schmidt@uni-koeln.de>
Date:   Fri Mar 6 16:53:29 2020 +0100

    hdbscan Text

[33mcommit 17e343106878ae20bc2ad553627cdc6629579638[m
Merge: 4f6f8e8 a591552
Author: SCSchmidt <s.c.schmidt@uni-koeln.de>
Date:   Fri Mar 6 16:39:05 2020 +0100

    Merge branch 'master' of https://gitlab.com/oliver.nakoinz/klassifikator

[33mcommit 4f6f8e83cb58440f8e8fc15b89e0690a7aef1db4[m
Author: SCSchmidt <s.c.schmidt@uni-koeln.de>
Date:   Fri Mar 6 16:38:48 2020 +0100

    R-markdown: wichtigste Punkte

[33mcommit c11da6d890f20c59f346c17e80c3a6755f08d7b4[m
Author: SCSchmidt <s.c.schmidt@uni-koeln.de>
Date:   Fri Mar 6 15:46:45 2020 +0100

    Kommentar zu Rmarkdown entfernt

[33mcommit a59155296a3e97cb30a1a4a385599ec26da2bd1d[m
Author: Oliver Nakoinz <oliver.nakoinz@mail.de>
Date:   Fri Mar 6 15:44:41 2020 +0100

    fuzzy

[33mcommit 8cb1c19a05f97cf81cb587370df1dbd27f47a63c[m
Author: SCSchmidt <s.c.schmidt@uni-koeln.de>
Date:   Fri Mar 6 15:44:06 2020 +0100

    hDBscan intro in slides fertig. vermutlich.

[33mcommit 68c636ced44c049c300e9a1e0593762d4421f511[m
Author: Oliver Nakoinz <oliver.nakoinz@mail.de>
Date:   Fri Mar 6 14:58:34 2020 +0100

    Einleitung

[33mcommit 657dbc5faad216a59b3a3375c0c9bd01c91ab01e[m
Author: SCSchmidt <s.c.schmidt@uni-koeln.de>
Date:   Fri Mar 6 14:27:23 2020 +0100

    hdBscan html und slidy begonnen

[33mcommit f9d92cc5f3d2240c305a1dc38fbcfd0f7f7fb8ca[m
Author: SCSchmidt <s.c.schmidt@uni-koeln.de>
Date:   Thu Mar 5 19:03:58 2020 +0100

    R markdown Einführung begonnen

[33mcommit b122e826f52fc1f11457bd6b2a9dfdb7d6ecc879[m
Author: SCSchmidt <s.c.schmidt@uni-koeln.de>
Date:   Thu Mar 5 18:06:15 2020 +0100

    Rstudio und git

[33mcommit 1ca2ec1d969373e15276e365e93c15a557aa8e19[m
Author: SCSchmidt <s.c.schmidt@uni-koeln.de>
Date:   Thu Mar 5 17:04:08 2020 +0100

    weiter intro

[33mcommit 291c9ba3a3dd6cb577a48eb0622e1203c77750bf[m
Author: SCSchmidt <s.c.schmidt@uni-koeln.de>
Date:   Thu Mar 5 16:54:13 2020 +0100

    Einführung git begonnen

[33mcommit bb71851843cea4143c8b8a6a4987e7ef73e29648[m
Author: OliverNakoinz <oliver.nakoinz@mail.de>
Date:   Wed Mar 4 21:32:59 2020 +0100

    readme details

[33mcommit bde80a855c588c52cf328dbb2be357104b59e112[m
Author: OliverNakoinz <oliver.nakoinz@mail.de>
Date:   Wed Mar 4 21:31:16 2020 +0100

    readme

[33mcommit 44d3d0c11eeb9b5a00150d64ef49d5d08402b05f[m
Author: onakoinz <oliver.nakoinz.i@gmail.com>
Date:   Wed Feb 19 13:33:21 2020 +0100

    gitignore

[33mcommit 3363d05ca85aa7086822468c27d180863ca878f5[m
Author: onakoinz <oliver.nakoinz.i@gmail.com>
Date:   Wed Feb 19 10:46:13 2020 +0100

    gitignore
    participants only local

[33mcommit 526473f35fa8eb91189b0b21d08e9fd9d486dc8e[m
Author: Oliver Nakoinz <oliver.nakoinz@mail.de>
Date:   Thu Nov 14 10:35:22 2019 +0100

    readme korrekturen

[33mcommit 767383c23da1b4c54b4f42a155b93e7ed2382109[m
Author: Oliver Nakoinz <oliver.nakoinz@mail.de>
Date:   Thu Nov 14 10:18:34 2019 +0100

    readme korrekturen den daten

[33mcommit 83a0b717d272fe1ec23abae1fac7ce0a0796ef51[m
Author: Oliver Nakoinz <oliver.nakoinz@mail.de>
Date:   Thu Nov 14 10:14:34 2019 +0100

    readme korrekturen

[33mcommit 2020c6d135a3a52979376824a2d1933cf50b2cdf[m
Author: Oliver Nakoinz <oliver.nakoinz@mail.de>
Date:   Thu Nov 14 10:01:45 2019 +0100

    readme

[33mcommit 000dd6c020c237d9ba91daa8e87b24883a541cb7[m
Author: Oliver Nakoinz <oliver.nakoinz@mail.de>
Date:   Thu Nov 14 09:54:48 2019 +0100

    flyer details

[33mcommit 5cdb0f1044e487da690f21f55dd3cedb15af3509[m
Merge: 62777a1 0073d37
Author: OliverNakoinz <oliver.nakoinz@mail.de>
Date:   Tue Nov 5 11:13:39 2019 +0000

    Merge branch 'patch-1' into 'master'
    
    Tippfehler und kleine Satzumstellungen in der README
    
    See merge request oliver.nakoinz/klassifikator!1

[33mcommit 0073d37bcc729e72270bfa35c99c47ef86959327[m
Author: Sophie Schmidt <idhreneth@gmail.com>
Date:   Tue Nov 5 10:47:48 2019 +0000

    Tippfehler und kleine Satzumstellungen in der README

[33mcommit 62777a112ef6ef171df1ad57d7e15a027312e374[m
Author: OliverNakoinz <oliver.nakoinz@mail.de>
Date:   Mon Nov 4 20:51:24 2019 +0100

    ort

[33mcommit 1074de14af142dba7aaa5042ea920024492381c6[m
Author: Oliver Nakoinz <oliver.nakoinz@mail.de>
Date:   Mon Nov 4 18:01:10 2019 +0100

    details im README

[33mcommit c5a8a1a1bdd954554e6431510e26a65a8f9ee904[m
Author: Oliver Nakoinz <oliver.nakoinz@mail.de>
Date:   Mon Nov 4 17:54:30 2019 +0100

    links

[33mcommit 0a1880733962f7d98c7668cf4558c5ce2faeae43[m
Author: Oliver Nakoinz <oliver.nakoinz@mail.de>
Date:   Mon Nov 4 17:52:26 2019 +0100

    Initial commit
