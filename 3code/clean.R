library(magrittr)
library(dplyr)
library(glue)
library(stringr)

# Read file names

tibble(fig =  dir("../4figures"), path = "../4figures", used = 0) %>% 
    dplyr::bind_rows(tibble(fig =  dir("../4figures/icons"), path = "../4figures/icons", used = 0)) -> 
    fnames_fig

fnames_fig %>% 
    glue_data('{path}/{fig}')  %>% 
    as.character() -> fnames_fig$path2

fnames_fig$fig <- str_split(fnames_fig$fig, "[.]",simplify = T)[,1]
#fnames_fig$fig <- sub("[.]", "\\\\.",fnames_fig$fig)

tibble(scr =  dir("6praes"), path = "../6praes") %>% 
    dplyr::bind_rows(tibble(scr =  dir("."), path = ".")) %>% 
    dplyr::bind_rows(tibble(scr =  dir("../5documents_pub"), path = "5documents_pub")) ->
    fnames_script

fnames_script %<>% 
    glue_data('{path}/{scr}') %>% 
    tibble() %>% 
    rename("name" = ".")

fnames_script %<>% 
    dplyr::filter(stringr::str_detect(name, ".\\.Rmd|.\\.rmd|.\\.r|.\\.R|.\\.md"))


# Read files and mark figures

for(sfile in pull(fnames_script)){
    text <- readr::read_file(as.character(sfile))
    for (i in seq_along(fnames_fig$fig)){
        if (stringr::str_detect(text, fnames_fig$fig[i]) == T) {fnames_fig$used[i] <- 1}
    }
}
              

# Remove files

for (i in seq_along(fnames_fig$fig)){
#    if (fnames_fig$used[i] == 0) {file.remove(as.character(fnames_fig$path2[i]))}
}




