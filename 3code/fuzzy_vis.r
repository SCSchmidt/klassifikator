# Unscharfe Clustervisualisierung
## Vorbereitung
library(archdata)
data("Fibulae")
#Zuweisung der Zeilennamen (Grave_Mno), Löschen der nicht genutzten Merkmale und erneute Betrachtung der Datenstruktur (Kontrolle).
row.names(Fibulae)<- paste(Fibulae$Grave, Fibulae$Mno ,sep="_")
euklid_df <- Fibulae[, -which(names(Fibulae) %in% c("Grave","Mno","BFA","FA","BRA","Coils"))]
#Zeilen mit fehlenden (NA) Werten entfernen und erneute Betrachtung der Datenstruktur (Kontrolle).
euklid_df <- subset(euklid_df, FEL > 0.0)
euklid_df <- euklid_df[complete.cases(euklid_df),]
# Skalieren (Z-Transformation) der gesamten Datentabelle
euklid_scaled_df <- scale(euklid_df, center = F)


## kmeans
fib_euk_km <- kmeans(euklid_scaled_df, centers = 2)

# Distanzen der Objekte zu den Clustern
fib_euk_km_obj_memb <- matrix(data = 0, nrow = nrow(euklid_scaled_df), ncol = nrow(fib_euk_km$centers))

row.names(fib_euk_km_obj_memb) <- row.names(euklid_scaled_df)

for (i in 1:nrow(fib_euk_km$centers)){
    center <- fib_euk_km$centers[i,]
    for (j in 1:nrow(euklid_scaled_df)) {
        object <- euklid_scaled_df[j,]
        fib_euk_km_obj_memb[j,i] <- sqrt(sum((center-object) ^ 2))
    }
}

# Zugehörigkeit der Objekte zu den Clustern
for (i in 1:nrow(fib_euk_km_obj_memb)) {
    object <- fib_euk_km_obj_memb[i,]
    dsum   <- sum(object)
    object <- dsum - object
    object <- object / dsum
    fib_euk_km_obj_memb[i,] <- object
}

# Heatmap fuzzy Membership der Objekte zu den Clustern
heatmap(fib_euk_km_obj_memb)


# Cluster und Zuordnungsgrad
cluster_as <- function(x){
    which(x==max(x))
}
fib_euk_km_clusas <- apply(fib_euk_km_obj_memb, 1, FUN = cluster_as)
fib_euk_km_memb <- apply(fib_euk_km_obj_memb, 1, FUN = max)

# pca mit größe

fib_euk_pca <- princomp(euklid_scaled_df)
fib_euk_pca_2dim <- fib_euk_pca$scores[,1:2]
plot(fib_euk_pca_2dim,
     pch = 16,
     col = fib_euk_km_clusas + 1,
     cex = (10*fib_euk_km_memb^4),
     main = "Cluster membership")

