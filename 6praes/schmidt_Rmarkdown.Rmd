---
title: "Kurze R Markdown Einführung"
author: "Sophie C. Schmidt"
date: "5 3 2020"
output: word_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE,
                      message = FALSE,
                      warning = FALSE)
```

Markdown ist eine einfache Auszeichnungssprache, mit der in Textdokumenten mit einfachen Symbolen eine Layoutvorgabe kodiert werden kann. Mithilfe von Pandoc und LaTeX lassen sich diese Dokumente dann in unterschiedliche Ausdrucke umwandeln, z. B. docx, pdf oder html.

R Markdown bietet die komfortable Möglichkeit, innerhalb eines Markdown - Dokuments Codeblöcke zu integrieren und diese zusammen mit dem Text

  a) zu speichern
  b) in das erwünschte Endformat umzuwandeln
  
Das heißt, es kann spezifiziert werden, welche Code-Blöcke ausgeführt werden, welche im Text als Code angezeigt werden, wie groß die Grafiken sein sollen, die durch den Code entstehen etc pp.

Sehr sehr praktisch. Die Pakete, die wir benötigen heißen: 
  
      Rmarkdown
      knitr 
Also bitte einmal:

```{r, echo=TRUE, include=TRUE}
#install.packages("Rmarkdwon")
library(rmarkdown)
#install.packages("knitr")
library(knitr)
```


Ein paar einfache Dinge zum Aufbau:

Ein Rmd-Dokument beginnt immer mit einem sogenannten YAML-Header. Dort werden wichtige Daten hinterlegt. Mindestens ein Titel, der Autor, das Datum und in welche Art Dokument das Rmd transfomiert werden soll. So ein YAML-"Kopf" beginnt und endet mit drei Bindestrichen: " - - - "

Schaut einmal in den Klassifikator > 6praes - Ordner, dort liegen mehrere Rmd-Dateien mit unterschiedlichen YAML-Köpfen. Könnt ihr erraten, welche Datei in was für ein Format überführt werden soll?

Als nächstes folgt in der Regel ein Code-Chunk der ein paar globale Regeln festlegt, wie Code chunks in diesem Dokument behandelt werden. Er beginnt mit:
" \```{r setup, include=FALSE} " und endet mit " ``` ". 

Diese Symbole " \```{r} " lassen den Umwandler wissen, das hier jetzt Code beginnt, der ausgeführt werden soll. " ``` " bezeichnen stets das Ende eines Code-Blocks.

Der Befehl für das Set-up ist 
    
    knitr::opts_chunk$set()

und die wichtigsten Optionen für die Code-Blöcke sind vermutlich diese:

    include = FALSE bedeutet, dass weder Code noch die Ergebnisse im fertigen Dokument erscheinen. Der Code wird trotzdem ausgeführt und kann anderweitig genutzt werden
    echo = FALSE bedeutet, dass die Ergebnisse angezeigt werden, aber nicht der Code. So kann man z.B. Graphen besonders gut einfügen
    message = FALSE verhindert, dass Nachrichten, die vom Code erstellt werden, im fertigen Dokument angezeigt werden
    warning = FALSE verhindert, dass Warnungen, die vom Code produziert werden, im fertigen Dokument angezeigt werden
    fig.cap = "..." erstellt eine Bildunterschrift für Code-Blöcke, die Bilder produzieren.

Alle diese Optionen sind sowohl global für alle Code-Blöcke nützlich als auch für einzelne in den Text eignefügte Blöcke.

Für die Formatierung von Text stehen die typischen Markdown-Symbole zur Verfügung:

![Cheatsheet für R Markdown: https://rstudio.com/wp-content/uploads/2016/03/rmarkdown-cheatsheet-2.0.pdf ](https://d33wubrfki0l68.cloudfront.net/65dffd1bdcaa0025006262164d98e8068e8b4387/c3895/wp-content/uploads/2018/08/rmarkdown-2.0.png)

\# 
für die Markierung von Überschriften

\- beginnt Listen

1. beginnt nummerierte Listen

\!\[\](/Pfad/zu/Bild.jpg) kann Bilder einfügen, Links mi

Der genaue "style" von Dokumenten kann außerhalb von dem Rmd-Dokument mithilfe von templates und/oder style CSS-Dokumenten festgelegt werden. 

Tabellen lassen sich auf unterschiedliche Art und Weisen formatieren. Nützlich ist z. B. das Paket kable, welches dann innerhalb eines Code-Blocks ausgeführt wird. Es geht aber auch über einfache Linien:

\| Right | Left | Default | Center | 

\|------:|:-----|---------|:------:| 

\|   12  |  12  |    12   |    12  |

\|  123  |  123 |   123   |   123  | 

\|    1  |    1 |     1   |     1  | 

werden zu:

| Right | Left | Default | Center | 
|------:|:-----|---------|:------:| 
|   12  |  12  |    12   |    12  |
|  123  |  123 |   123   |   123  | 
|    1  |    1 |     1   |     1  | 


Innerhalb des normalen Textes lässt sich R code aufrufen und ausführen in dem man \'r hier kommt der Code hin \' schreibt. Der Output kann dann so aussehen: `r 7 + 10`  (wie man sieht, sieht man nur das Ergebnis).

Mathematische Formeln lassen sich als LaTeX-Code integrieren, in dem man sie zwischen Dollarzeichen setzt: \$hier kommt Code hin\$. Das Ergebnis kann so aussehen: $\sum_{p \in \mathrm{cluster}} (\lambda_p - \lambda_{\mathrm{birth}})$


Es lassen sich Zitationen einfügen, was sehr praktisch sein kann. Man nutzt eine Literaturdatenbank im bib-Format, verlinkt diese im YAML ("bib: Pfad/zu/der/.bib") und kann dann das gewünschte Zitat mit einem [\@short_cut] einfügen. Die Struktur des KlassifikatoR-Projekts legt die Bibliographie im ORdner "7lib/71citations" ab. Desweiteren finden sich unter "7lib/72csl" diverse "citation style language"-Dokumente, die dabei behilflich sind, die Bibliographie auf die Art und Weise zu formatieren, wie wir möchten. Wird die csl im YAML-Header verlinkt ("csl: /Pfad/zur/gewünschten/csl.csl"), übernimmt sie die Formatierung sowohl der in-line-Zitate als auch der Bibliographie am Ende.



