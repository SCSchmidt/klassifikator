---
title: "x"
output: html_document
---
## Gower

# Mixed Data

## Introduction
In many cases, a dataset will not just include binary data, but also a mixture of metric, ranked, categorical and binary data. For example, in the dataset `Michelsberg', the variable "mbk_phase" is ranked with the order I < I/II < II < II/III < III < III-V < III/IV < IV < IV/V < Munz < V while (in the previous section) we had changed the observations of counts of ceramic types to presence/absence data. This section provides a guide for how to create a distance measure in such cases.



## Data Preparation

For the example we can use the Michelsberg - data and re-attach the ranked categorial information on the phases, just so that we have some ranked data in the data set. 

```{r}
df_gow <- data.frame(Michelsberg$mbk_phase, newdf) 
```

## Gower Distance

The Gower Distance can be used for mixed data sets, incorporating metric and ranked data as well as categorical and binary data. We use the "Functional Diversity" R package called `FD`, in which the calculation of Gower Distance has been implemented by Etienne Laliberté, Pierre Legendre and Bill Shipley [-@FD]. The function is named `gowdis()`.

The equation for the algorithm implemented in this function is

$G_{jk} = ∑^n_{i=1}w_{ijk}s_{ijk} / ∑^n_{i=1}w_{ijk}$

where
$w_{ijk}$ is the weight of variable i for the j-k pair, and

$s_{ijk}$ is the partial similarity of variable i for the j-k pair, and where

$w_{ijk}= 0$ if objects j and k cannot be compared because $x_{ij}$ or $x_{ik}$ is unknown (i.e. NA) . 

For binary variables, $s_{ijk} = 0$ if $x_{ij}!= x_{ik}$, and $s_{ijk} = 1$ if $x_{ij} = x_{ik} = 1$ or if $x_{ij} = x_{ik} = 0$.

For nominal variables, $s_{ijk} = 0$ if $x_{ij} != x_{ik}$ and $s_{ijk} = 1$ if $x_{ij} = x_{ik}$.

This means, Gower Distance uses the simple matching algorithm with Hamming Distance for the categorical and binary data.

For the ordinal variable types all values ($x_{ij}$) are replaced by their ranks ($r_{ij}$) determined over all objects (such that ties are also considered), and then if ord = "podani" $s_{ijk} = 1$ if $r_{ij} = r_{ik}$

Distance is 

D = 1 - Difference of the values divied by their range.

In ranked categorical data, ties may occur. There are three options how to handle this and the following "podoni"-option is preferred, it was proposed by Podani [-@podani_1999] and it can be chosen as `ord = podani` in the function:

Tied values are replaced by average ranks, as shown in the equation. 
$s_{12j} = 1 – [|y_{1j} – y_{2j}| / R_j ]$

Make sure that distances between adjacent states are comparable in magnitude. So steps between rank 1 and rank 2 should be similar to the difference between step 2 and 3. Legendre suggests to convert the ranked categorial data into unordered factors should there be too much difference. 

Now we load the FD package and include this very easy line of code:
```{r}
library(FD)
Mb_gow <- gowdis(df_gow, ord = "podani")
```

Tadaaa!
As in all the other cases, `Mb_gow` is a distance table of the class dist in R:

```{r}
class(Mb_gow)
```


## Anzahlen
