---
title: "Klassifikation: partitionierende Clusteranalyse"
author:
- affiliation: Johanna Mestorf Akademie / Institut für Ur- und Frühgeschichte, Christian-Albrechts
    Universität zu Kiel
  email: oliver.nakoinz@ufg.uni-kiel.de
  name: '[PD Dr. Oliver Nakoinz](http://oliver.nakoinz.gitlab.io/OliverNakoinz/)'
- affiliation: Institut für Ur- und Frühgeschichte, Christian-Albrechts Universität
    zu Kiel
  email: sofnod@googlemail.com
  name: Michael Bilger
- affiliation: Exzellenzcluster ROOTS, Christian-Albrechts-Universität zu Kiel
  email: rplath@roots.uni-kiel.de
  name: Romy Vanessa Plath
- affiliation: Institut für Prähistorische Archäologie, Freie Universität Berlin
  email: Georg.Roth@fu-berlin.de
  name: Georg Roth
- affiliation: SFB 1266 "Scales of Transformation" / Institut für Ur- und Frühgeschichte, Christian-Albrechts Universität zu Kiel
  email: julian.laabs@ufg.uni-kiel.de
  name: Julian Laabs
date: "Version 1, 2020-03-10 <br> '<a href='https://creativecommons.org/licenses/by-sa/4.0/' target='_blank' class='class1'>License</a> <a href='https://creativecommons.org/licenses/by-sa/4.0/' target='_blank' class='class2'>CC BY-SA 4.0</a>'"
output:
  html_document:
    css: ../3code/tutorials.css
    df_print: kable
    highlight: tango
    toc: yes
    toc_depth: 5
    toc_float: yes
csl: ../7lib/72csl/harvard-european-archaeology.csl
lang: en-GB.UTF-8
number_sections: yes
otherlangs: de-DE
bibliography: 
  - ../7lib/71citations/bib_gruppe1.bib
  - ../7lib/71citations/lit_packages_g1.bib
pandoc_args: --filter=pandoc-citeproc
self_contained: yes
smart: yes
Comment: xxx
---
<!--
Fuzzy is not included anymore,
begin not relevant
-->
## Clusteralgorithmus: fuzzy clustering

Unscharfe Clusterung (Fuzzy clustering) verwendet keine scharfe Zuweisung von Objekten zu Clustern sondern weist jedes Objekt mit einem Zugehörigkeitsgrad jedem Cluster zu. Die Zugehörigkeitsgrade werden sowohl bei der Bewertung der Clusterlösungen mit Hilfe einer Zielfunktion, als auch im Ergebnis der Clusterung verwendet. 

### Fuzzy Analysis Clustering mit fanny

Wenn eine Distanzmatritz als Eingabe übergeben wird, sollte keine Metrik angegeben werden. Wenn Rohdaten übergeben werden, dann ist eine Spezifizierung der Metrik (des Distanzmaßes) essentiell.

```{r}
library(cluster)

euklid_fanny <- cluster::fanny(x=euklid_df,
                               metric="eu",
                               k=3)

head(euklid_fanny$membership)
```

Zur Visualisierung des Ergebnisses eine graphische Darstellung:

```{r}
clusplot(euklid_fanny)
```

### Fuzzy k-means

Als Alternative bietet sich Fuzzy k-means an, das aus dem Paket fclust stammt. Hier werden die Rohdaten übergeben, nicht die Distanzmatrizen!

```{R}
library(fclust)
euklid_fkm <- fclust::FKM(X=euklid_df, 
                    k=3)
head(euklid_fkm$U)
```

```{r}
plot(euklid_fkm, pca = TRUE)
```

###  Gustafson and Kessel - like fuzzy k-means

```{r}

euklid_fkm.gk <- fclust::FKM.gk(X=euklid_df, 
                    k=3, RS = 5)
head(euklid_fkm.gk$U)
```

Und wieder visualieren.

```{r}
plot(euklid_fkm.gk, pca = TRUE)
```

### Jetzt für Gower

```{r}
gower_fanny <- cluster::fanny(x=gower_dist,
                              diss = T,
                              k=3)

head(gower_fanny$membership)
```

Zur Visualisierung des Ergebnisses eine graphische Darstellung:

```{r}
clusplot(gower_fanny, color=T)
```

### Anzahl

Wir können auf die Anzahldaten, die mit Decostand transformiert worden sind, alle Verfahren anwenden, die auch in dem Euklidischen Beispiel verwandt worden sind. Wir werden hier als Beispiel nur die fuzzy k-means für alle drei Versionen durchführen.

```{r}
chi_fkm <- fclust::FKM(X=anzahl_decostand_chi, 
                    k=2)
cho_fkm <- fclust::FKM(X=anzahl_decostand_cho, 
                    k=2)
hell_fkm <- fclust::FKM(X=anzahl_decostand_hell, 
                    k=2)
```

Und wir schauen uns das Ergebnis der unterschiedlichen Distanzmaße im Plot an:

```{r}
plot(chi_fkm, pca = TRUE)
plot(cho_fkm, pca = TRUE)
plot(hell_fkm, pca = TRUE)
```

<!--
Fuzzy is not included anymore,
end not relevant
-->