---
title: "Distance measurement for binary and nominal data"
author: 
  - affiliation: Universität zu Köln
    email: s.c.schmidt uni-koeln.de
    name: '[Sophie C. Schmidt](https://www.archaeoinformatics.net)'
  - affilitation: Yale University
    email: sarah.martini@yale.edu
    name: '[Sarah Martini]()'
  - affilitation: École Normale Supérieure Paris
    email: carole.quatrelivre@gmail.com
    name: '[Carole Quatrelivre]()'
  - affilitation: Christian-Albrechts-Universität zu Kiel
    email: rstaniuk@gshdl.uni-kiel.de
    name: '[Robert Staniuk](https://gitlab.com/rstaniuk)'
date: "Version 1, 2020-03-10" 
output:
  html_document:
    css: ../3code/tutorials.css
    df_print: kable
    highlight: tango
    toc: yes
    toc_depth: 5
    toc_float: yes
csl: ../7lib/72csl/harvard-european-archaeology.csl
lang: en-GB.UTF-8
number_sections: yes
otherlangs: de-DE
bibliography: 
 - ../7lib/71citations/lit.bib
 - ../7lib/71citations/lit_packages.bib
pandoc_args: --filter=pandoc-citeproc
self_contained: yes
smart: yes
Comment: xxx
---

```{r setup, message=FALSE, warning=FALSE, include=FALSE}
library("RColorBrewer")
library("knitr")
knitr::opts_chunk$set(
    message = FALSE,
    warning = FALSE,
    fig.align = 'center',
    fig.pos = 'H',
    #fig.path = "../4figures/pdf/",
    #dev = c("png"),
    dpi = 300,
    tidy = TRUE
)
```


<input type="button1" class="button1" value="EN">
<input type="button2" class="button2" value="R">
<input type="button3" class="button3" value="Static">
<input type="button4" class="button4" value="">

# Metadata 

## Abstract

Distance measurement allows identification of the underlying principles in multivariate datasets. This tutorial shows how to generate distance measurement for archaeological datasets. The problem is to select the algorithm suitable for the type of data used in the investigation. This tutorial focuses specifically on nominal data.

Four different methods of distance calculations are used in this tutorial:

1. Simple matching coefficient
2. Jaccard index
3. Ochiai coefficient
4. Hamming distance.


```{r ER diagram, echo=FALSE, message=FALSE, warning=FALSE, fig.cap = ""}
library(DiagrammeR)  # http://rich-iannone.github.io/DiagrammeR/index.html   http://rich-iannone.github.io/DiagrammeR/graphviz_and_mermaid.html
dfgraph1 <- DiagrammeR::grViz("workflow_ENG.gv", width = 600, height = 700, engine = "dot") # engine = "neato")
dfgraph1
```

## R Packages and Resources

The `archdata` package provides the example for conducting the analysis [@carlson_2017]. The example dataset is mostly "Michelsberg". The distance matrix calculation for simple matching, Jaccard coefficient and Ochiai is provided by `ade4` [@ade4]. The hamming distance matrix is calculated using the `FD` package [@FD].


<label class="trigger">
  <input type="checkbox" class="checkbox checkbox--red" /> Additional help for installing the packages
  <span class="msg">
    install.packages("archdata","ade4","FD","bibtex")
     </span>
</label>


## Data

We are using the `Michelsberg` dataset from the package `archdata` . This dataset includes counts of 39 vessel types from 109 archaeological features belonging to 69 sites of the Central European Younger Neolithic Michelsberg Culture (MBK; 4350-3500 BC) and one site of the Funnel Beaker Culture (TBK 4300 - 2800 BC). Additional information includes the Lüning (@Luning1967) phase association of each assemblage along with its XY coordinates (UTM WGS 84 Zone 32N), Site Name, Catalogue Number (@hohn_2002), and Feature Number.   


```{r loading the dataset}
library(archdata)
data("Michelsberg")
```


## Funding

This tutorial was developed by the authors for the [Johanna Mestorf Academy](http://www.jma.uni-kiel.de/en?set_language=en) and the [Institute of Pre- and Protohistoric Archaeology](https://www.ufg.uni-kiel.de/en?set_language=en) of Kiel University for the use in the teaching of quantitative archaeology.
On content level the [ISAAK](https://isaakiel.github.io) team supported the tutorial with numerous ideas and inputs. 
The development of this tutorials was funded by [PerLe-Fonds für Lehrinnovation](https://www.perle.uni-kiel.de/de/pb/fonds)

![](../4figures/Perlelogos/Logos.svg)

---

# Binary Data

## Introduction
For binary data, disimilarity (D) and similarity (S; D = 1-S) are measured based on the counts of four cases:

- Case a = number of shared "presences" (cell value =1)
- Case b = number of presences (1s) found only in Object 1
- Case c = number of presences (1s) found only in Object 2
- Case d = number of shared "absences" (cell value = 0)


Let us take the example of two objects for which the presence/absence of six variables was observed:

| | Variable 1 | Variable 2 | Variable 3 | Variable 4 | Variable 5 | Variable 6 |
|:----:|:----:|:----:|:----:|:----:|:----:|:----:|
|Object 1| 1 | 1 | 0 | 1 | 0 | 0 |
|Object 2| 1 | 0 | 1 | 0 | 0 | 0 |

For this example,

- Case a = 1
- Case b = 2
- Case c = 1
- Case d = 2


The equations presented in the distance measures discussed below draw upon these four cases.

## Preparation
Your first step to distance measuring, is ensuring that you have the right dataset. The Michelsberg dataframe used in this tutorial offers abundance variables as well as nominal and ordinal variables. However, we only wish to measure the distance between objects on the basis of the presence and/or absence of types. 

As we create a new dataframe, we will only select abundance variables.
```{r preparation binary}
newdf <- Michelsberg[,5:39]
```

Then, we need to transform the count values into a presence value : 1.
```{r transformation binary}
newdf [newdf > 0] <- 1
```

The dataframe is now ready to be tested. 


## Simple matching Distance

The simple matching coefficient is used to measure dissimilarity when the dataset is symmetrical. For symmetrical binary data both "0" and "1" represent meaningful classifications. Historically the most commonly cited example is gender as male and female. In archaeological research such datasets are uncommon, due to the higher number of variables encountered in research (we may classify gender as male or female, but always have to add the category "not identifiable"). Hence in this tutorial we include the necessary steps for analyzing such datasets but will not perform the analysis on dataset. Should you know or use symmetrical binary data, we would love to hear back from you so we could modify the analysis.

The formula for simple matching Distance of symmetrical binary data is as follows: 

$$
SMC = \frac{a+d}{a+b+c+d}
$$

This can be transfered to a distance measure by substracting it from 1.

$$
D_{SMC} = 1 - \frac{a+d}{a+b+c+d}
$$

It does not apply weight to samples.

To apply the SM distance, we use the package "ade4" and select SM by passing 2 to the method argument. Please note that function produces the square root of SM distance.

```{r simple matching, results = "hide"}
library(ade4)
dist.binary(newdf, method = 2)
```

## Jaccard index resp. Jaccard distance
The Jaccard similarity coefficient can be used for binary datasets, based on the number of shared presences between two objects. Considering :
- a, the number of shared variables by 2 objects
- b, the number of variables owned by 1st object, but not shared with the 2nd
- c, the number of variables owned by 2nd object, but not shared with the 1st

then $S = \frac{a}{(a + b + c)}$ or $D = 1 - (\frac{a}{(a + b + c)})$

Please note, that shared absences (O's resp. quantity *d*) are omitted, that is, the Jaccard distance is an asymmetric binary distance measure.

To apply the Jaccard, we pass 1 to the method argument of ade4 package : 

```{r jaccard coeff}
dist.binary (newdf, method = 1)
```

```{r jaccard coeff results, echo=FALSE}
jc <- dist.binary(newdf, method = 1)
mjc <- as.matrix(jc)
mjc[1:4, 1:4]
```

Please note that function ` ade4::dist.bin()` produces the square root of Jaccard distance.

## Ochiai
In comparison with the Jaccard index, the Ochiai weights the number of shared presences (quantity *a*) based on their amount of overlap. This weighting is implemented by scaling with the geometric mean of the quantities *a+b* and *a+c*

Thus,

$$
D = 1 - \frac{a} {\sqrt{(a+b)*(a+c)}}
$$

To apply Ochiai, we pass 7 to the method argument of ade4 package : 

```{r ochiai coeff}
dist.binary (newdf, method = 7)
```

```{r ochiai coeff results, echo=FALSE}
oc <- dist.binary(newdf, method = 7)
moc <- as.matrix(oc)
moc[1:4, 1:4]
```

Please note that function again produces the square root of the original distance.

## Hamming Distance


The Hamming distance is more or less a "simple matching" for **categorical data**. Categorical data means that not just "0"s and "1"s are possible, but describing categories of data, which are not numerical and can not be ranked. Examples would be colour described by words ("red", "blue", "orange") or - very archaeologically - types of pottery. 

### data

Because this is a distance measure for categorical data, we have to use another data set. We will take the data set called `PitHouses` from the `archdata` package. It consists of the description of six morphological featuresof Late Stone Age and Early Sami Iron Age Pithouses in Arctic Norway, described by Engelstad (1988).

```{r}
library(archdata)
data("PitHouses")
```


Using Hamming distance calculates the similarity to be 1 ($S_k = 1 $) if the values in the column k are the same, if they are not, the value is 0.
The sum of these values is divided by the number of columns that are considered. This way the sum gets normalized:

$ S_{total} = \frac{\sum S_k} {n\ of\ columns}$

The Hamming distance then is, as usual, 1 minus the similarity value $S_total$.
$D = 1 – S_{total}$

There are several packages in R, which calculate this distance measure. We suggest to use the package `FD` and the function called `gowdis()`. It implements the Gower Distance measure, which is applicable to several data types at the same time. For categorical / qualitative descriptors it uses the described Hamming distance. 

In it a missing value automatically changes the weight $w_j$ to 0 [@legendre_2012, 280], which means that the similarity of two objects that do not share a feature is, for this column, 0.


You apply the Hamming distance using this code:

```{r hamming distance}
library(FD)
PH_hamming <- gowdis(PitHouses)

```

To look at the distance matrix we convert it into a matrix and call upon the first 4 rows and columns.
```{r look at hamming}

PH_ham <- as.matrix(PH_hamming)
PH_ham[1:4, 1:4]
```

# Conclusion

Well done! You have now learned about different distance matrices. It is important to think about what kind of data you have (binary or categorical) and how you want to weight the co-occurence of absence of a type. Is this important in your case? Or is it rather because your dataset may be incomplete?

Think about these topics before using distance matrices and cluster analysis methods!

---

# References
